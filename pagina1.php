<?php
$usuario = $_GET['usuario'];
$email = $_GET['email'];
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <div class="full-box">
        <h1>Recuperação 1 - HTML, CSS,<br> JavaScript, PHP</h1>
        <hr class='linha' />
    </div>
    <div class="full-box">
        <h2>Aluno: Lucas Dias Oliveira - Turma: AUT2D3</h2>
        <hr class='linha' />
    </div>
    <div id="error"></div>
    <form id="form_enviar" action="pagina1.php" method="GET">
        <div class="half-box">
            <h3>Dados Recebidos:</h3>
            <label id="respostas"><?php echo "O usuário é: ", $usuario ?></label>
        </div>
        <div>
            <label id="respostas2"><?php echo "O email é: ", $email ?></label>
        </div>
    </form>
    <a href="index.php">Voltar para a página inicial</a>     
</body>
</html>