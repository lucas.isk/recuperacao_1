<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
    <script defer src="scripts.js"></script>
</head>
<body>
    <div class="full-box">
        <h1>Recuperação 1 - HTML, CSS,<br> JavaScript, PHP</h1>
        <hr class='linha' />
    </div>
    <div class="full-box">
        <h2>Aluno: Aluno: Lucas Dias Oliveira - Turma: AUT2D3</h2>
        <hr class='linha' />
    </div>
    <div id="error"></div>
    <form id="form_enviar" action="pagina1.php" method="GET">
        <div class="half-box">
            <h3>Cadastro de Usuários</h3>
            <label for="usuario">Usuário: </label>
            <input id="usuario" name="usuario" type="text"> 
        </div>
        <div>
            <label for="email">E-mail: </label>
            <input id="email" name="email" type="email"> <br>
        </div>
        <div>
            <button>Enviar</button>
        </div>
    </form>     
</body>
</html>